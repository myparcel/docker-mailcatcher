ARG UBUNTU_VERSION=rolling

FROM ubuntu:${UBUNTU_VERSION}

ARG DEBIAN_FRONTEND="noninteractive"
ARG MAILCATCHER_VERSION="0.7.1"

    # Install Ruby and Mailcatcher build requirements 
RUN apt-get --yes --quiet update && \
    apt-get --yes --quiet install --no-install-recommends build-essential libsqlite3-0 libsqlite3-dev ruby ruby-dev && \
    # Install mailcatcher
    gem install mailcatcher --version ${MAILCATCHER_VERSION} && \
    # Clean up build requirements
    apt-get --yes autoremove --purge build-essential libsqlite3-dev ruby-dev && \
    # Cleanup up apt caches
    apt-get clean --yes && \
    rm --recursive --force /var/lib/apt/lists/* && \
    useradd --create-home --shell /bin/bash mailcatcher

USER mailcatcher
WORKDIR /home/mailcatcher

EXPOSE 1025
EXPOSE 1080

CMD ["mailcatcher", "--foreground", "--smtp-ip", "0.0.0.0", "--smtp-port", "1025", "--http-ip", "0.0.0.0", "--http-port", "1080"]
