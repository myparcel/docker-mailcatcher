# MailCatcher [![Docker Pulls](https://img.shields.io/docker/pulls/myparcel/mailcatcher.svg?style=flat)](https://hub.docker.com/r/myparcel/mailcatcher/) [![Docker Stars](https://img.shields.io/docker/stars/myparcel/mailcatcher.svg?style=flat)](https://hub.docker.com/r/myparcel/mailcatcher/) [![Docker Automated build](https://img.shields.io/docker/automated/myparcel/mailcatcher.svg?style=flat)](https://hub.docker.com/r/myparcel/mailcatcher/)

This is a Docker image for [MailCatcher](https://mailcatcher.me) which runs an SMTP server that catches any message sent
and allows them to be viewed in a simple web interface.


## Usage

To use this image, run it with `docker run`:

```bash
docker run --name='mailcatcher' -p 1080:1080 -p 1025:1025 myparcel/mailcatcher
```

Alternatively, if you use Docker Compose, you can add MailCatcher as a service:

```yaml
version: "3"

services:
  mailcatcher:
    image: myparcel/mailcatcher
    ports:
      - "1025:1025"
      - "1080:1080"
```
